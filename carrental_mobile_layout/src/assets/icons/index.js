import IconAkun         from './akun.svg'
import IconAkunAktif    from './akunAktif.svg'
import IconList         from './list.svg'
import IconListAktif    from './listAktif.svg'
import IconHome         from './home.svg'
import IconHomeAktif    from './homeAktif.svg'
import IconBox          from './box.svg'
import IconCamera       from './camera.svg'
import IconTruck        from './truck.svg'
import IconKey          from './key.svg'
import IconKoper        from './koper.svg'
import IconPenumpang    from './penumpang.svg'

export {IconAkun, 
        IconAkunAktif, 
        IconHome, 
        IconHomeAktif, 
        IconList, 
        IconListAktif,
        IconBox,
        IconCamera,
        IconKey,
        IconTruck,
        IconKoper,
        IconPenumpang }