import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'

const Banner = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.bgmobil}></Text>
            <Image
                style={styles.mobil}
                source={require('../../assets/img/mobil.png')}
            />
            <Text style={styles.sewa}>Sewa Mobil Berkualitas {"\n"} di Kawasanmu</Text>
            <Text style={styles.kotak}>
                <TouchableOpacity>
                    <Text style={styles.buttonText}>Sewa Mobil</Text>
                </TouchableOpacity>

            </Text>
        </View>
    )
}

export default Banner

const styles = StyleSheet.create({

    container: {
        // position: 'absolute',
        width: 376,
        height: 140,
        left: 16,
        top: 104,

        backgroundColor: '#091B6F',
        borderRadius: 8,
    },

    sewa: {
        width: 170,
        height: 48,
        left: 24,
        top: 24,

        color: '#FFFFFF',
        fontWeight: '400',
        fontSize: 16,
        lineHeight: 24,
    },

    kotak: {
        display: 'flex',
        position: 'absolute',
        width: 109,
        height: 28,
        left: 24,
        top: 88,

        borderRadius: 2,
        backgroundColor: '#5CB85F',
    },

    buttonText: {
        width: 77,
        height: 20,
        left: 16,
        top: 4,
        color: '#FFFFFF',

        fontWeight: '700',
        fontSize: 14,
        lineHeight: 20,
    },

    mobil: {
        position: 'absolute',
        width: 178,
        height: 103,
        left: 190,
        top: 37,
    },

    bgmobil: {
        position: 'absolute',
        backgroundColor: '#0D28A6',
        top: 78.5,
        width: 178,
        height: 61.5,
        left: 197.6,
        borderTopLeftRadius: 60,
        borderBottomRightRadius: 8,
    }
})