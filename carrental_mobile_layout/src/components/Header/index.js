import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const Header = () => {
  return (
    <View style={styles.header}>
      <View>
        <Text style={styles.nama}>Hi, Herlan</Text>
        <Text style={styles.lokasi}>Cimahi, Jawa Barat</Text>
      </View>

      <View>
        <Image
          style={styles.aku}
          source={require("../../assets/img/aku.jpg")} />
      </View>
    </View>
  )
}

export default Header

const styles = StyleSheet.create({

  header: {
    width: '100%',
    height: 176,
    position: 'absolute',
    backgroundColor: '#D3D9FD',
    top: -15,
    left: 0,
  },

  aku: {
    position: 'absolute',
    width: 28,
    height: 28,
    left: 350,
    top: 43,
    borderRadius: 35,
    overflow: 'hidden'
  },


  nama: {
    width: 90,
    height: 18,
    left: 18,
    top: 46,

    fontWeight: '300',
    fontSize: 12,
    lineHeight: 18,
    color: '#000000'
  },

  lokasi: {
    position: 'absolute',
    width: 150,
    height: 20,
    left: 18,
    top: 68,

    fontWeight: '700',
    fontSize: 14,
    lineHeight: 20,
    color: '#000000'
  }
})