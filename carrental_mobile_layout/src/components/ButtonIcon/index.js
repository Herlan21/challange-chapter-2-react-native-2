import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { IconKey, IconTruck, IconCamera, IconBox } from '../../assets'
import { WARNA_ICON } from '../../utils/constant';

const ButtonIcon = ({ title, type }) => {

  const Icon = () => {
    if (title === "Sewa Mobil") return <IconTruck />
    if (title === "Oleh Oleh") return <IconBox />
    if (title === "Penginapan") return <IconKey />
    if (title === "Wisata") return <IconCamera />

    return <IconTruck />
  }

  return (
    <TouchableOpacity style={{ position: 'absolute' }}>
      <View style={styles.kotak}>
        <View style={styles.button}>
          <Icon />
          <View >
            <Text style={styles.nama}>{title}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ButtonIcon

const styles = StyleSheet.create({
  kotak: {
    top: 100,
  },

  button: {

    width: 56,
    height: 56,
    left: 10.9,

    borderRadius: 8,
    backgroundColor: WARNA_ICON,
    justifyContent: 'center',
    flexDirection: 'row',
    margin: 20,
    paddingVertical: 15.5

  },

  nama: {
    position: 'absolute',
    top: 45,
    width: 100,
    left: -40,
    color: '#000000',
    alignItems: 'center',
    // justifyContent: 'space-evenly',

  },

})