import React, { useState } from "react";
import { FlatList, View, Image, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity } from "react-native";
import { MobilSewa } from "../../components/ListMobilSewa";
import { IconPenumpang, IconKoper } from "../../assets";

const ItemMobil = ({ item, onPress}) => ( 

  <View style={{backgroundColor: 'white'}}>
  
  <TouchableOpacity onPress={onPress} style={[styles.item]}>

  <Image style={styles.xenia} source={require("../../assets/img/daihatsu.png")} /> 

   <View>
    <View style={{marginLeft: 16, marginTop: 16}}>
    <Text style={styles.Mobil}>{item.Mobil}</Text>
    
    <View style={{flexDirection: 'row'}}>
    <IconPenumpang />
    <Text style={styles.kapasitas}>{item.Kapasitas}</Text>
    <IconKoper />
    <Text style={styles.barang}>{item.Barang}</Text>
    </View>
    <Text style={styles.harga}>{item.Harga}</Text>
    </View>
    </View>
    </TouchableOpacity>
    </View> 
);

  const AppSewa = () => {
  const [selectedId, setSelectedId] = useState(null);

  const renderItem = ({ item }) => {
    const backgroundColor = item.Harga === selectedId ? "#FFFFFF" : "#f9c2ff";
    const color = item.Harga === selectedId ? 'white' : 'black';

    return (
      <ItemMobil
      item={item}
      onPress={() => setSelectedId(item.Harga)}
      backgroundColor={{ backgroundColor }}
      textColor={{ color }}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={MobilSewa}
        renderItem={renderItem}
        keyExtractor={(item) => item.Harga}
        extraData={selectedId}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   marginTop: StatusBar.currentHeight || 0,
  //   backgroundColor: '#FFFFFF'
  // },

  item: {
    marginHorizontal: 16,
    marginTop: 16,
    flexDirection: 'row',
    padding: 16,
    color: '#000000',
    backgroundColor: 'white',

    shadowColor: "#000",
    shadowOffset: {
	  width: 0,
	  height: 2,
},
shadowOpacity: 0.23,
shadowRadius: 2.62,

elevation: 4,
  },

  kapasitas: {
    color: '#000000',
    fontSize: 12
  },

  barang: {
    color: '#000000'
  },

  Mobil: {
    color: '#000000'
  },

  harga: {
    color: '#000000'
  },

  xenia: {
    marginVertical: 20,
  },
  
  title: {
    fontSize: 32,
  },
});

export default AppSewa






















// import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
// import { AppSewa } from '../../components/'


// const Mobil = () => {
//   return (
//     <View>
//       <View>
//       <Text style={styles.judul}>Daftar Mobil</Text>
//       </View>
//         <AppSewa />
        
//       <View>

//       </View>
//     </View>
//   )
// }

// export default Mobil

// const styles = StyleSheet.create({

//   judul: {
//     top: 48,
//     left: 20,
//     fontSize: 20,
//     color: '#000000',
//     fontWeight: '700',
//     position: 'absolute',
    
//   }
// })