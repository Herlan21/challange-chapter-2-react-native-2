import React from 'react'
import { StyleSheet, Text, View, Image,TouchableOpacity,StatusBar } from 'react-native'



const Akun = () => {
  return (
   

      <View style={{backgroundColor: 'white', flex: 1}}>
        <View >
          <Text style={styles.judul}>Akun</Text>
          <Image
            style={styles.background}
            source={require("../../assets/img/akun.png")} />
          <Text style={styles.notifikasi}>Upss Kamu belum memiliki akun, Mulai buat akun agar transaksi di BCR lebih mudah</Text>

          <View>
            <TouchableOpacity>
              <Text style={styles.tombol}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    
  )
}

export default Akun

const styles = StyleSheet.create({

  container: {

  },

  judul: {
    color: '#000000',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 20,

    width: 50,
    height: 20,
    left: 25,
    top: 48
  },

background: {
  position: 'absolute',
  width: 312,
  height: 192,

  top: 270,
  left: 48,
},

notifikasi: {
  width: 312,
  top: 450,
  left: 48,
  color: '#000000',
  fontWeight: '400',
  textAlign: 'center',
  lineHeight: 20,
  fontSize: 14
},

tombol: {
  width: 81,
  height: 36,
  left: 170,
  top: 470,
  paddingTop: 8,
  color: '#FFFFFF',
  borderRadius: 2,
  fontWeight: '700',
  textAlign: 'center',
  display: 'flex',
  backgroundColor: '#5CB85F',
}
})