import React from 'react'
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, ScrollView } from 'react-native'
import { Header, Banner, MobilPilihan, ButtonIcon } from '../../components/'

const Home = () => {
  return (
        <View>
          <View>
            <Header />
            <Banner />
                 <Text style={styles.iconLayanan}>
                <ButtonIcon title="Sewa Mobil" type="layanan" />
                <ButtonIcon title="Oleh Oleh" type="layanan" />
                <ButtonIcon title="Penginapan" type="layanan" />
                <ButtonIcon title="Wisata" type="layanan" />
                </Text>     
            </View>
            
          </View>
  )
}

export default Home
const styles = StyleSheet.create({

  iconLayanan: {
    top: 10,
},

container: {
  width: '100%',
  height: 700,
  flex: 1,
},


})