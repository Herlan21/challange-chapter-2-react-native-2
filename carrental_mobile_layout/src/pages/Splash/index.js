import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import {mobil} from '../../assets'

const Splash = ({navigation}) => {
  
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    },3000)
  }, [navigation]);
   
  return (
    <View style={styles.container}>
      <Text style={styles.judul}>BCR {"\n"} Binar Car Rental</Text>
      
      <View style={styles.card} >
      <ImageBackground source= {mobil} style={styles.mercedes}></ImageBackground>
    </View>
  </View>
  )
}

export default Splash

const styles = StyleSheet.create({

container: {
 position: 'relative',
 width: 360,
 height: 640,
 backgroundColor: '#091B6F'
},

  mercedes: {
    position:'absolute',
    width: 360,
    height: 208,
    bottom: 0,
    
  },

  card: {
    position: 'absolute',
    backgroundColor: '#D3D9FD',
    width: 360,
    height: 124,
    left: 0,
    top: 470,
    borderTopLeftRadius: 60
  },

  judul: {
    position: 'absolute',
    width: 189,
    height: 72,
    left: 85,
    top: 230,
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily: 'Helvetica',
    lineHeight: 36,
    fontSize: 24,
    fontWeight: 'bold',
    fontStyle: 'normal',

  }
})